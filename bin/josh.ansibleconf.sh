#
#     ____.            .__        ___ ___         __    _____.__       .__       .___
#    |    | ____  _____|  |__    /   |   \_____ _/  |__/ ____\__| ____ |  |    __| _/
#    |    |/  _ \/  ___/  |  \  /    ~    \__  \\   __\   __\|  |/ __ \|  |   / __ | 
#/\__|    (  <_> )___ \|   Y  \ \    Y    // __ \|  |  |  |  |  \  ___/|  |__/ /_/ | 
#\________|\____/____  >___|  /  \___|_  /(____  /__|  |__|  |__|\___  >____/\____ | 
#                    \/     \/         \/      \/                    \/           \/ 
#
# ansibleconfig, Run before running openshift cluster script.


##################
### setup auth ###
##################
auth ()
{
echo "Enter AWS Access key ID:"
read AWSID
sleep 5s
echo "Enter AWS SECRET ACCESS KEY:"
read AWSSEC
sleep 5s

touch ~/.aws_creds

echo "export AWS_ACCESS_KEY_ID='$AWSID'" > ~/.aws_creds
echo "export AWS_SECRET_ACCESS_KEY='$AWSSEC'" >> ~/.aws_creds
source ~/.aws_creds
}

setup ()
{
################################
#### Install applications  #####
################################

yum install -y ansible python-boto pyOpenSSL git vim tmux wget


###################
### Setup vars  ###
###################

export ec2_vpc_subnet='subnet-2a6a504f'
export ec2_image='ami-61bbf104'
export ec2_region='ap-southeast-2'
export ec2_keypair='puppet'
export ec2_security_groups="['openshift-jh-build']"
export ec2_assign_public_ip='true'
export os_etcd_root_vol_size='20'
export os_etcd_root_vol_type='standard'
export os_etcd_vol_size='20'
export os_etcd_vol_type='standard'
export os_master_root_vol_size='20'
export os_master_root_vol_type='standard'
export os_node_root_vol_size='15'
export os_docker_vol_size='50'
export os_docker_vol_ephemeral='false'

##########################
### Instrance settings ###
##########################

#### All instances:

export ec2_instance_type='t2.small'

#### Master instances:

#export ec2_master_instance_type='m4.large'

#### Infra node instances:

#export ec2_infra_instance_type='m4.large'

#### Non-infra node instances:

#export ec2_node_instance_type='m4.large'

#### etcd instances:

#export ec2_etcd_instance_type='m4.large'
}


if [[ $1 == run ]]; then
	setup
fi

if [[ $2 == auth ]]; then
	auth
fi


